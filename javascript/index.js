var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    
    //set game tick and read car infos
    gameTick = data.gameTick;
    var car = getCar(data);

	//read lane
	lane = car.piecePosition.lane.startLaneIndex;

	//read laneAddToRadius
	for (var i = 0; i < 4; i++)
	{
		if (lanes[i].index == lane)
		{
			laneAddToRadius = lanes[i].distanceFromCenter;
			break;
		}
	}

	//set index
    if (car.piecePosition.pieceIndex != pieceIndex)
    {
		pieceIndex = car.piecePosition.pieceIndex;
		if (index == -1)
			index = pieceIndex;
		else
			index++;
		if (car.piecePosition.lap != lap)
	    {
	        lap = car.piecePosition.lap;
			//console.log('lap: ' + lap);
	    }
		
		console.log();
		console.log('----------------lap: ' + lap + ', pieceIndex: ' + pieceIndex + ', INDEX: ' + index + '-----------------');
		var pie = pieces[getPieceIndexFromIndex(index)];
		if (pie.radius && pie.angle)
			console.log('' + index + ' curve: ' + pie.angle);
		else
			console.log('' + index + ' straight');

		console.log();
		//console.log('lap: ' + lap + ', pieceIndex: ' + pieceIndex + ', Kurvensteilheit: ' + kurvensteilheit());
	}
    
    //calculate currentVelocity
	var newInPieceDistance = car.piecePosition.inPieceDistance;
	if (inPieceDistance < 0)
		inPieceDistance = newInPieceDistance;
	if (newInPieceDistance > 0 && newInPieceDistance > inPieceDistance)
	{
        currentVelocity = newInPieceDistance - inPieceDistance;
	}
	else
	{
		var pieceLength = 0;
		var p = pieces[getPieceIndexFromIndex(index - 1)];
		if (p.radius && p.angle) 
		{
			if (p.angle < 0)	//left turn
			{
				pieceLength = (p.radius + laneAddToRadius) * Math.PI * Math.abs(p.angle) / 180;
			}
			else				//right turn
			{
				pieceLength = (p.radius - laneAddToRadius) * Math.PI * Math.abs(p.angle) / 180;
			}
		}
		else 
		{	
			pieceLength = p.length;
		}
		currentVelocity = newInPieceDistance + (pieceLength - inPieceDistance);
	}
	inPieceDistance = newInPieceDistance;
    
    //calculate max. velocity
    var myTimesGConst = 0.43;	//0.46
	var count = 3;
	var maxV = 10;
	for (var i = 0; i < count; i++)
	{
		var newMaxV = 10;
		var p = pieces[getPieceIndexFromIndex(index + i)];
		if (p.radius && p.angle) 
		{
			if (p.angle < 0)	//left turn
			{
				newMaxV = Math.sqrt((p.radius + laneAddToRadius) * myTimesGConst);//konst. 10	0.46	//mit secur:0.48
				console.log('left turn (' + p.angle	+ ') newMaxV: ' + newMaxV);
			}
			else				//right turn
			{
				newMaxV = Math.sqrt((p.radius - laneAddToRadius) * myTimesGConst);//konst. 10	0.46	//mit secur:0.48
				console.log('right turn (' + p.angle + ') newMaxV: ' + newMaxV);
			}
		}
		else 
		{	
			newMaxV = 10;
			console.log('straight');
		}
		if (newMaxV < maxV)
		{
			//if (currentVelocity >= newMaxV)	//jetzt schneller als untersuchte Höchstgeschwindigkeit
			{
				if (i == 0)
				{
					maxV = newMaxV;
				}
				else
				{
					//Bremsweg berechnen
					//var breakLength = (currentVelocity * currentVelocity - newMaxV * newMaxV) * 0.5 / myTimesGConst;
					var breakLength = (currentVelocity - newMaxV) * 49.49831637;
					console.log('--> index: ' + index
						 + ', breakLength: ' + breakLength
						 + ', getLengthToIndex(' + (index + i) + '): ' + getLengthToIndex(index + i));
					if (breakLength + 2 * currentVelocity >= getLengthToIndex(index + i))
						maxV = newMaxV;
				}
			}
		}
	}
    
    //keep maxV
	if (maxV == 10 || currentVelocity < maxV)
	{
		throttle(1.0);
		console.log('speed: ' + currentVelocity + ' < ' + maxV + ' THROTTLE: 1');
	}
	else
	{
		throttle(0.0);
		console.log('speed: ' + currentVelocity + ' > ' + maxV + ' THROTTLE: 0');
	}

	console.log('');


//    send({
//      msgType: "throttle",
//      data: 0.6
//    });
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameInit') {
      pieces = data.data.race.track.pieces;
      lanes = data.data.race.track.lanes;
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
		var resultsIndex = 0;
		for(; resultsIndex < cars.length; resultsIndex++)
		{
			if (data.data.bestLaps[resultsIndex].car.name === botName)//'turkinpippuri')
			    break;
		}
      var result = data.data.bestLaps[resultsIndex].result;
      console.log('Race ended. ' + 'lap: ' + result.lap + ', ticks: ' + result.ticks + ', millis: ' + result.millis);
    } else if (data.msgType === 'dnf') {
	  if (data.data.car.name == botName)
        console.log('Disqualified (reason: ' + data.data.reason + ')');
    } else if (data.msgType === 'crash') {
	  if (data.data.name == botName) console.log('Crash');

		throw new Error();

    } else if (data.msgType === 'spawn') {
	  if (data.data.name == botName) console.log('Spawn');
	}

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});

var index = -1;
var gameTick = 0;
var lane = 0;
var laneAddToRadius = 0;
var pieceIndex = -1;
var lap = -1;
var inPieceDistance = -1;
var currentVelocity = 0;

var pieces;
var lanes;
var cars;

function getLengthToIndex(ind)
{
	var coveredLength = 0;
	for (var i = index; i < ind; i++)
	{
		var p = pieces[getPieceIndexFromIndex(i)];
	    if (p.angle && p.radius)
		{
			var curvelength = 0;
			if (p.angle < 0)	//left turn
			{
				curvelength = 2 * (p.radius + laneAddToRadius) * Math.PI * Math.abs(p.angle) / 360;
			}
			else				//right turn
			{
				curvelength = 2 * (p.radius - laneAddToRadius) * Math.PI * Math.abs(p.angle) / 360;
			}
			
			if (i == 0)	curvelength -= inPieceDistance;
			coveredLength += curvelength;
		}
		else
		{
			var straightlength = p.length;
			if (i == 0) straightlength -= inPieceDistance;
			coveredLength += straightlength;
		}
	}
	return coveredLength;
}

//count = pieces to look //forward
function sumAnglePieces(count)
{
	//var start_i = 0 - Math.floor(count / 2);
	var angle = 0;
	for (var i = 0 /*start_i*/; i < count/* - start_i*/; i++)
	{
		var p = pieces[getPieceIndexFromIndex(index + i)];
	    if (p.angle)
		{
			angle += Math.abs(p.angle);
		}
	}
	return angle;
}

function getPieceIndexFromIndex(ind)
{
	var pieceInd = ind;
	if (pieceInd < 0)
		pieceInd += pieces.length;
	while(pieceInd >= pieces.length)
		pieceInd -= pieces.length;
	return pieceInd
}

function getCar(data)
{
	cars = data.data;
    var carindex = 0;
    for(; carindex < cars.length; carindex++)
    {
		console
        if (data.data[carindex].id.name === botName)//'turkinpippuri')
            break;
    }
    return data.data[carindex];
};

function throttle(value)
{
	send({
	    msgType : "throttle",
	    data : value,
	    gameTick : gameTick
	});
	//console.log('current throttle: ' + value);
}

function switchLane(direction)
{
	if (direction === 'Left' || direction === 'Right')
	{
		send({
			msgType : "switchLane",
			data : direction
			//gameTick : gameTick
		});
		console.log('switch: ' + direction);
	}
}